//Import the http module using the required directive.

const http = require('http');


//Create a variable port and assign it with the value of 3000

const port = 3000;



//Create a server using createServer method that will listen in to the port provided above.

// Create a condition that when the login route is accessed, it will print a message to the user that they are in the login page. 


// Access the login route to test if it's working as intended


// Create a condition for any other routes that will return an error message

// Access any other route to test if it's working as intended.


const server = http.createServer((request, response) => {

	if (request.url == '/login') {
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Welcome to the login page!');
	}
	else {
		response.writeHead(404, {'Content-Type': 'text/plain'});
		response.end('Page not available');
	}

})


//Console log in the terminal a message when the server is successfully running

server.listen(port);

console.log(`Server now accessible at localhost:${port}.`);



